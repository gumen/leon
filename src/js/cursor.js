;(function (window, document) {
  var cursor = document.getElementById('cursor')
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)

  if (isMobile) {
    cursor.remove()
  } else {
    document.body.classList.add('custom-cursor')
    window.addEventListener('mousemove', function (event) {
      cursor.style.transform = 'translate3d(' + event.x + 'px,' + event.y + 'px,0.001px)'
    })
  }
})(window, document)
