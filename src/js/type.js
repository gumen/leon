;[].forEach.call(document.querySelectorAll('[data-type]'), function (el) {
  var texts = el.dataset.type.split(';')
  var speed = +el.dataset.speed || 200
  var delay = +el.dataset.delay || 3000

  var t = 0                     // Text index
  var l = 0                     // Letter index

  function clear (length) {
    if (length) {
      el.textContent = el.textContent.slice(0, el.textContent.length - 1)
      setTimeout(clear.bind(null, length -1), Math.floor(speed / 8))
    } else {
      t++; if (!texts[t]) t = 0
      setTimeout(type, speed)
    }
  }

  function type () {
    if (texts[t][l]) {
      el.textContent += texts[t][l]
      l++
      setTimeout(type, speed)
    } else {
      setTimeout(clear.bind(null, texts[t].length), delay)
      l = 0
    }
  }

  setTimeout(type, speed)
})
