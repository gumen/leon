;(function (window, document) {
  var fps = 30
  var lastTime = 0
  var curretnTime = 0
  var duration = 3000

  function randomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  var dots = [].map.call(document.querySelectorAll('.dot'), function (dot) {
    return {
      el : dot,
      ct : 0,                    // current time
      f  : true,                 // forward?
      y  : randomInt(-50, 50),   // end Y
      x  : randomInt(-10, 10),     // end X
      d  : randomInt(2000, 6000) // duration
    }
  })

  function animationLoop (time) {
    window.requestAnimationFrame(animationLoop)

    if (time - lastTime >= 1000 / fps) {
      lastTime = time

      dots.forEach(function (dot) {
        dot.ct += 1000 / fps

        if (dot.ct >= dot.d) {
          if (!dot.f) { // End of full cycle
            dot.y = randomInt(-50, 50)
            dot.x = randomInt(-10, 10)
          }

          dot.d = randomInt(2000, 6000)
          dot.f = !dot.f
          dot.ct = 0
        }

        var y = Easing.get(
          'easeInOutQuad',      // type
          dot.f ? 0 : dot.y,    // start value
          dot.f ? dot.y : 0,    // end value
          dot.ct,               // curent animation time
          dot.d)                // animation duration

        var x = Easing.get(
          'easeInOutQuad',      // type
          dot.f ? 0 : dot.x,    // start value
          dot.f ? dot.x : 0,    // end value
          dot.ct,               // curent animation time
          dot.d)                // animation duration

        dot.el.style.transform = 'translate(' + x + 'px,' + y + 'px)'
      })
    }
  }

  window.requestAnimationFrame(animationLoop)
})(window, document)
