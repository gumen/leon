;(function (window, document) {

  var lightbox = document.getElementById('lightbox')
  var imageContainers = document.querySelectorAll('#single .wp-block-image')

  var buttons = {
    close    : lightbox.querySelector('svg.close'),
    next     : lightbox.querySelector('svg.arrow.right'),
    previous : lightbox.querySelector('svg.arrow.left'),
  }

  var lightboxImages = []
  var imageIndex = 0            // Currently visible image in lightbox

  /**
   * This very confusing name means that lightbox will be shown
   * depending on url hash.  If hash is #lightbox then show it.
   */
  function showHideLightbox () {
    lightbox.hidden = location.hash !== '#lightbox'
  }

  /**
   * Show lightbox with chosen image
   * @param {Number} i - index of image to show
   */
  function showLightbox (i) {
    lightboxImages[imageIndex].hidden = true
    if (i >= lightboxImages.length) i = 0
    if (i < 0) i = lightboxImages.length - 1
    imageIndex = i
    lightboxImages[imageIndex].hidden = false

    // Show lightbox by setting proper hash
    if (lightbox.hidden) location.hash = 'lightbox'
    showHideLightbox()
  }

  function hideLightbox  () { window.history.back() }
  function nextImage     () { showLightbox(imageIndex + 1) }
  function previousImage () { showLightbox(imageIndex - 1) }


  // Show lightbox on hashchange
  window.addEventListener('hashchange', showHideLightbox)
  showHideLightbox()            // Initial check

  // Disabl image links created by WordPress
  ;[].forEach.call(imageContainers, function (container) {
    var link = container.querySelector('a')
    if (link) link.addEventListener('click', function (event) {
      event.preventDefault()
    })
  })

  // Show lightbox by clicking on images
  ;[].forEach.call(imageContainers, function (container, i) {
    var image = container.querySelector('img')
    var newImage = image.cloneNode()
    newImage.hidden = i !== imageIndex
    lightbox.appendChild(newImage)
    lightboxImages.push(newImage)
    image.addEventListener('click', showLightbox.bind(null, i))
  })

  // Close lightbox by clicking on background
  lightbox.addEventListener('click', function (event) {
    if (event.target.id === 'lightbox') hideLightbox()
  })

  // Handle buttons
  buttons.close    .addEventListener('click', hideLightbox)
  buttons.next     .addEventListener('click', nextImage)
  buttons.previous .addEventListener('click', previousImage)

  // handle keyboard
  window.addEventListener('keydown', function (event) {
    switch (event.code) {
      // Close lightbox by clicking Esc key
    case 'Escape': if (!lightbox.hidden) lightbox.hidden = true ;break

      // Change to next image
    case 'ArrowRight':
    case 'ArrowDown': nextImage() ;break

      // Change to previous image
    case 'ArrowLeft':
    case 'ArrowUp': previousImage() ;break
    }
  })

})(window, document)
