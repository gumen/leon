const fse  = require('fs-extra')
const path = require('path')

const srcPath  = 'src'
const distPath = 'dist'
const tmpPath  = 'tmp'
const wpPath   = '/var/www/html/leon/wp-content/themes/leon'

const buildType = process.argv[2] || 'full'

console.info('Build type:', buildType)

;(async function () {
  if (buildType === 'full') {
    await fse.remove(distPath)    ;console.log('Remove last build')
    await fse.ensureDir(distPath) ;console.log('Create directory:', distPath)
    await fse.ensureDir(tmpPath)  ;console.log('Create directory:', tmpPath)
  }

  if (buildType === 'full') {
    const filesList = [
      'style.css',
      'fonts',
      'images',
      'favicon.png',
      'screenshot.png',
    ]

    // Copy
    for (const file of filesList) {
      await fse.copy(path.join(srcPath, file), path.join(distPath, file))
      console.log('Copy to dist:', file)
    }
  }

  // Styles
  {
    const postcss = require('postcss')
    const plugins = ['postcss-encode-background-svgs']
    const filesList = [
      'main',
      'single',
      'front-page',
      'page-about',
      'page-contact',
      'page-process',
      'index',
      'dots',
      'intro',
      'cursor',
      'lightbox',
    ]

    if (buildType === 'full') {
      plugins.push('autoprefixer')
      plugins.push('cssnano')
    }

    for (const file of filesList) {
      const from = path.join(srcPath, `styles/${file}.css`)
      const to   = path.join(tmpPath, file + '.css')

      const css = await fse.readFile(from)
      const processor = postcss(plugins.map(plugin => require(plugin)))
      const result = await processor.process(css, { from, to })

      await fse.writeFile(to, result.css)
      console.log('Create styles in:', to)
    }
  }

  // Scripts
  {
    const uglifyjs = require('uglify-js')
    const options = { ie8: true }

    const filesList = [
      'type',
      'dots',
      'cursor',
      'lightbox',
    ]

    for (const fileName of filesList) {
      const from = path.join(srcPath, 'js', fileName + '.js')
      const to = path.join(tmpPath, fileName + '.min.js')
      const code = await fse.readFile(from, 'utf8')
      const result = uglifyjs.minify(code, options)

      if (!result.code) {
        console.warn(result.warnings)
        console.error(result.error)
      } else {
        await fse.writeFile(to, result.code)
        console.log('Create scripts in:', to)
      }
    }
  }

  // Templates
  {
    const nunjucks = require('nunjucks')
    const env = nunjucks.configure({
      autoescape: false,
      trimBlocks: true,
      lstripBlocks: true
    })
    const filesList = [
      'index',
      'single',
      'front-page',
      'page-about',
      'page-contact',
      'page-process',
      'functions',
      // '404',
    ]

    for (const file of filesList) {
      const from = path.join(srcPath, file + '.nunjucks')
      const to   = path.join(distPath, file + '.php')
      const html = nunjucks.render(from)

      await fse.writeFile(to, html)
      console.log('Build template from:', from, 'to:', to)
    }
  }

  // Copy to WordPress themes directory
  {
    if (buildType === 'full') {
      await fse.copy(distPath, wpPath)
      console.log('\'dist\' copied to:', wpPath)
    } else {
      const filesList = [
        'index.php',
        'single.php',
        'front-page.php',
        'page-about.php',
        'page-contact.php',
        'page-process.php',
        'functions.php',
        // '404.php',
      ]

      for (const file of filesList) {
        const from = path.join(distPath, file)
        const to   = path.join(wpPath, file)

        await fse.copy(from, to)
        console.log('File:', from, 'coppied to:', to)
      }
    }
  }
})()
